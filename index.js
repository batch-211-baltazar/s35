const express = require("express");
//Mongoose is a package that allows creation of Schemas to model our data structures
//Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

//MongoDB connection
//connect to the database by passing in our connection string, remember to replace the password and database names with actual values
//{newUrlParser:true} allows us to avoid any current and future errors while connecting to MongoDB

//Syntax
	//mongoose.connect("<MongoDB connection string>",{useNewUrlParser:true})

//Connecting to MongoDB Atlas

mongoose.connect("mongodb+srv://admin123:admin123@project0.cutbola.mongodb.net/s35?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology: true
	}
);

//Set notifications for connection success or failure
//connection to the database
//allows us to handle errors when the initial connection is established
//works with the on and once Mongoose methods
let db = mongoose.connection;

db.on("error",console.error.bind(console,"connection error"));
//if a connection error occured, output in the console
//console.error.bind(console) allows us to print errors in our terminal

db.once("open",()=>console.log("We're connected to the cloud database"))

//Mongoose Schemas
//Schemas determine the structure of the documnets to be written in the database
//Schemas act as blueprints to our data
//use the Schema() constructor of the Mongoose module to create a new Schema object
//the "new" keyword creates a new Schema
const taskSchema = new mongoose.Schema({
	//define the fields with their corresponding data type

	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

//Models
//uses schemas and are used to create/instatiate objects that correspond to the schema
//Server>Schema(blueprint)>Database>Collection

const Task = mongoose.model("Task",taskSchema);

//Setup for allowing the server to handle data from requests
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Create a new task
//Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		-if the task already exists in the database, we return an error
		-if the task does not exist in the database, we add it in the database
	2. The task data will be coming from the request's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks",(req,res)=>{

	Task.findOne({name:req.body.name},(err,result)=>{
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		}else{
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr,savedTask)=>{
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New task created");
				}
			})
		}
	})



})

app.get("/tasks",(req,res)=>{
	Task.find({},(err,result)=>{
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data : result
			})
		}
		
	})
})

//Activity



const signupSchema = new mongoose.Schema({
	//define the fields with their corresponding data type

username: String,
password: String
})

const Signup = mongoose.model("Signup",signupSchema);


app.post("/signup",(req,res)=>{

	Signup.findOne({username:req.body.username},(err,result)=>{
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate user found");
		}else{
			let newSignup = new Signup({
				username: req.body.username,
				password: req.body.password

			});
			newSignup.save((saveErr,savedSignup)=>{
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New user created");
				}
			})
		}
	})

})

//Stretch Goal

app.get("/signup",(req,res)=>{
	Signup.find({},(err,result)=>{
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data : result
			})
		}
		
	})
})

app.listen(port,() => console.log(`Server running at port ${port}`))